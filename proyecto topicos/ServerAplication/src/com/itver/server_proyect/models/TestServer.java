
package com.itver.server_proyect.models;

import com.itver.server_proyect.database.ConnectionDatabase;
import com.itver.server_proyect.database.MysqlUserDAO;
import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TestServer {
    
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int port=9090;
//        System.out.println("PUERTO DE SERVIDOR: ");
//        port = input.nextInt();
        try {
            MysqlUserDAO manager = new MysqlUserDAO(ConnectionDatabase.getConnection());
            LogicServer logicServer = new LogicServer(manager);
            Server server = new Server(port, logicServer);
        } catch (IOException ex) {
            Logger.getLogger(TestServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
